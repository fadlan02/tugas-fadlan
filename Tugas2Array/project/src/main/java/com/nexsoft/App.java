package com.nexsoft;

import java.util.ArrayList;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        System.out.println("tugas Menghapus data yang sama dari kedua array dan menambahkan data yang berbeda");
        // Array List 1
        ArrayList<String> arrayFood1 = new ArrayList<>();
        arrayFood1.add("ayam");
        arrayFood1.add("ikan");
        arrayFood1.add("telur");
        arrayFood1.add("lele");
        arrayFood1.add("gorengan");

        // Array List 2
        ArrayList<String> arrayFood2 = new ArrayList<>();
        arrayFood2.add("nasi");
        arrayFood2.add("ikan");
        arrayFood2.add("ayam");
        arrayFood2.add("sayur");
        arrayFood2.add("gorengan");

        // mengambil semua data dari arrayFood 1
        ArrayList<String> newArrayFood1 = new ArrayList<>(arrayFood1);
        // menambahkan semua data dari arrayFood2 ke newArrayFood1
        newArrayFood1.addAll(arrayFood2);
        // System.out.println("makanan" + newArrayFood1);

        // mengambil semua data dari arrayFood 1 ke NewArrayFood2
        ArrayList<String> newArrayFood2 = new ArrayList<>(arrayFood1);
        // membandingkan jika ada kesamaan pada arrayfood 2 maka akan di simpan
        newArrayFood2.retainAll(arrayFood2);
        // System.out.println("makanan :" + NewArrayFood2);

        // menghapus data dari NewArrayFood2
        newArrayFood1.removeAll(newArrayFood2);
        // System.out.println(newArrayFood1);

        // menghapus data dari arrayFood1
        arrayFood1.removeAll(arrayFood1);

        // // menambahkan data dari newArrayFood1 ke arrayFood1
        arrayFood1.addAll(newArrayFood1);
        System.out.println("makanan :" + arrayFood1);
    }
}
