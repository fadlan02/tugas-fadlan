package com.nexsoft;

class Data {
    private int id;
    private int parkingCardNumber;
    private String nama;
    private int salary;

    // Constructor
    Data(int id, int parkingCardNumber, String nama, int salary) {
        this.id = id;
        this.parkingCardNumber = parkingCardNumber;
        this.nama = nama;
        this.salary = salary;
        System.out.println("=====================================");
        System.out.println("id              : " + id);
        System.out.println("nomor parkir    : " + parkingCardNumber);
        System.out.println("nama            : " + nama);
        System.out.println("salary          : " + salary);

    }
}

public class App {
    public static void main(String[] args) {
        Data data1 = new Data(1, 10, "karyawan1", 3000000);
        Data data2 = new Data(2, 11, "karyawan2", 3000000);
        Data data3 = new Data(3, 12, "karyawan3", 3000000);
        Data data4 = new Data(4, 13, "manager1", 10000000);
        Data data5 = new Data(5, 14, "manager2", 10000000);
    }
}